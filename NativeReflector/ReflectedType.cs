﻿using System.Collections.Generic;
using System.Linq;

using Dia2Lib;

namespace NativeReflector
{
    /// <summary>
    /// Represents a type reflected from the PDB
    /// </summary>
    public class ReflectedType
    {
        /// <summary>
        /// Gets the name of this type
        /// </summary>
        public string Name { get; private set; }

        // All methods
        private IList<ReflectedFunction> methods;

        /// <summary>
        /// Gets all methods in this type
        /// </summary>
        public IEnumerable<ReflectedFunction> Methods { get { return methods; } }

        /// <summary>
        /// Gets the number of methods in this type
        /// </summary>
        public int MethodCount { get { return methods.Count; } }

        // All base classes
        private IList<ReflectedTypeRef> baseClasses;

        /// <summary>
        /// Gets all types that this type derives
        /// </summary>
        public IEnumerable<ReflectedTypeRef> BaseClasses { get { return baseClasses; } }

        /// <summary>
        /// Gets the number of types that this type derives
        /// </summary>
        public int BaseClassCount {  get { return baseClasses.Count; } }

        /// <summary>
        /// Gets if this type only contains pure virtual methods
        /// </summary>
        public bool IsInterface { get; private set; }

        /// <summary>
        /// Gets the size of this type when instantiated in bytes
        /// </summary>
        public int Size { get; private set; }

        /// <summary>
        /// Initializes a new instance of the ReflectedType class
        /// </summary>
        /// <param name="symbol"></param>
        public ReflectedType(IDiaSymbol symbol)
        {
            // Store name
            Name = symbol.name;

            // Load details
            LoadDetails(symbol);
        }

        #region Loading

        /// <summary>
        /// Loads all details
        /// </summary>
        private void LoadDetails(IDiaSymbol symbol)
        {
            // Get size
            Size = (int)symbol.length;

            // Find methods
            methods = new List<ReflectedFunction>();
            foreach (IDiaSymbol methodSymbol in symbol.EnumerateChildren(SymTagEnum.SymTagFunction))
            {
                ReflectedFunction method = new ReflectedFunction(this, methodSymbol);
                if (!methods.Any(e => e.Signature.ParametersMatch(method.Signature, true) && e.Name == method.Name))
                    methods.Add(method);
            }

            // Load base classes
            List<ReflectedTypeRef> baseClassList = new List<ReflectedTypeRef>();
            foreach (IDiaSymbol baseClass in symbol.EnumerateChildren(SymTagEnum.SymTagBaseClass))
            {
                baseClassList.Add(new ReflectedTypeRef(baseClass.type));
            }
            baseClasses = baseClassList.ToArray();

            // Determine if we're an interface
            IsInterface = Size == 0 && methods.Count > 0;
            if (IsInterface)
            {
                foreach (ReflectedFunction method in methods)
                {
                    if (method.Type == ReflectedMethodType.InstanceFunction || method.Type == ReflectedMethodType.OperatorOverload)
                    {
                        if (!method.IsPure || !method.IsVirtual)
                        {
                            IsInterface = false;
                            break;
                        }
                    }
                }
            }
        }

        #endregion

        public override string ToString()
        {
            return $"ReflectedType[{Name}]";
        }
    }
}
