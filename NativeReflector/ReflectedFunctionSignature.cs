﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

using Dia2Lib;

namespace NativeReflector
{
    /// <summary>
    /// Represents a function signature
    /// </summary>
    public class ReflectedFunctionSignature
    {
        /// <summary>
        /// Gets the return type of this function signature (null if void)
        /// </summary>
        public ReflectedTypeRef ReturnType { get; private set; }

        /// <summary>
        /// Gets the parameters of this function signature
        /// </summary>
        public IEnumerable<ReflectedTypeRef> Parameters { get { return parameters; } }

        /// <summary>
        /// Gets the parameters of this function signature with no this if present
        /// </summary>
        public IEnumerable<ReflectedTypeRef> ParametersNoThis
        {
            get
            {
                if (HasThis)
                    return Parameters.Skip(1);
                else
                    return Parameters;
            }
        }

        /// <summary>
        /// Gets the parameter count of this function signature
        /// </summary>
        public int ParameterCount { get { return parameters.Length; } }

        /// <summary>
        /// Gets the calling convention of this function signature
        /// </summary>
        public CallingConvention CallingConvention { get; private set; }

        /// <summary>
        /// Gets if the first parameter of this function signature is a "this" pointer
        /// </summary>
        public bool HasThis { get; private set; }

        /// <summary>
        /// Gets a C-style representation of this signature
        /// </summary>
        public string CStyle
        {
            get
            {
                return $"{ReturnType}(*)({string.Join(", ", parameters.Select(p => p.ToString()))})";
            }
        }

        // The parameters to this function signature
        private ReflectedTypeRef[] parameters;

        /// <summary>
        /// Initializes a new instance of the ReflectedFunctionSignature class
        /// </summary>
        /// <param name="symbol"></param>
        public ReflectedFunctionSignature(IDiaSymbol symbol)
        {
            // Determine calling convention
            switch (symbol.callingConvention)
            {
                case 0x00:
                    CallingConvention = CallingConvention.Cdecl;
                    break;
                case 0x04:
                    CallingConvention = CallingConvention.FastCall;
                    break;
                case 0x07:
                    CallingConvention = CallingConvention.StdCall;
                    break;
                case 0x09:
                    CallingConvention = CallingConvention.Winapi;
                    break;
                case 0x0b:
                    CallingConvention = CallingConvention.ThisCall;
                    break;
            }

            // Get return type
            ReturnType = new ReflectedTypeRef(symbol.type);

            // Get parameters
            List<ReflectedTypeRef> paramList = new List<ReflectedTypeRef>();
            IDiaSymbol objectPtrType = symbol.objectPointerType;
            if (objectPtrType != null)
            {
                paramList.Add(new ReflectedTypeRef(objectPtrType));
                HasThis = true;
            }
            //int paramCount = (int)symbol.count;
            foreach (IDiaSymbol arg in symbol.EnumerateChildren(SymTagEnum.SymTagFunctionArgType))
            {
                paramList.Add(new ReflectedTypeRef(arg.type));
            }
            parameters = paramList.ToArray();
        }

        /// <summary>
        /// Gets if this signature has the same parameter set as the other
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool ParametersMatch(ReflectedFunctionSignature other, bool skipThis)
        {
            if (ParameterCount != other.ParameterCount) return false;
            for (int i = skipThis ? 1 : 0; i < parameters.Length; i++)
            {
                if (!parameters[i].Equals(other.parameters[i]))
                    return false;
            }
            return true;
        }

        public override string ToString()
        {
            return CStyle;
        }
    }
}
