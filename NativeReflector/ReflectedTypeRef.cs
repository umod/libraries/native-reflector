﻿using System;

using Dia2Lib;

namespace NativeReflector
{
    /// <summary>
    /// Represents the type of a reference to a type
    /// </summary>
    public enum ReflectedTypeRefType
    {
        Type, Primitive, Pointer, Function
    }

    /// <summary>
    /// Represents a reference to a type
    /// </summary>
    public class ReflectedTypeRef : IEquatable<ReflectedTypeRef>
    {
        /// <summary>
        /// Gets the name of the type to which this reference refers
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the type of this reference
        /// </summary>
        public ReflectedTypeRefType RefType { get; private set; }

        /// <summary>
        /// If the referenced type is a pointer, gets the type it points to
        /// </summary>
        public ReflectedTypeRef BaseTypeRef { get; private set; }

        /// <summary>
        /// If the reference type is a function pointer, gets the signature of it
        /// </summary>
        public ReflectedFunctionSignature FunctionSignature { get; private set; }

        private static readonly string[] baseTypeMap = new string[]
        {
            "NOTYPE",
            "void",
            "char",
            "wchar_t",
            null, null, 
            "int",
            "uint",
            "float",
            null, // BCD
            "bool",
            null, null,
            "long",
            "unsigned long",
            null, null, null, null, null, null, null, null, null, null,
            null, // currency
            null, // date,
            null, // variant,
            null, // complex,
            null, // bit,
            null, // bstr,
            "HRESULT" // hresult
        };

        /// <summary>
        /// Gets a .Net compatible type
        /// </summary>
        public string NetType
        {
            get
            {
                switch (RefType)
                {
                    case ReflectedTypeRefType.Type:
                        return Name;
                    case ReflectedTypeRefType.Pointer:
                        return "IntPtr";
                    case ReflectedTypeRefType.Function:
                        return "IntPtr";
                    case ReflectedTypeRefType.Primitive:
                        switch (Name)
                        {
                            case "wchar_t":
                                return "char";
                            case "long":
                                return "int";
                            case "unsigned long":
                                return "uint";
                            case "HRESULT":
                                return "long";
                            default:
                                return Name;
                        }
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the ReflectedTypeRef class
        /// </summary>
        /// <param name="name"></param>
        public ReflectedTypeRef(string name)
        {
            // Store name
            Name = name ?? "";
        }

        /// <summary>
        /// Initializes a new instance of the ReflectedTypeRef class
        /// </summary>
        /// <param name="symbol"></param>
        public ReflectedTypeRef(IDiaSymbol symbol)
        {
            // Get the symtag
            SymTagEnum symTag = (SymTagEnum)symbol.symTag;
            switch (symTag)
            {
                case SymTagEnum.SymTagUDT:
                    Name = symbol.name;
                    RefType = ReflectedTypeRefType.Type;
                    break;
                case SymTagEnum.SymTagArrayType:
                case SymTagEnum.SymTagPointerType:
                    BaseTypeRef = new ReflectedTypeRef(symbol.type);
                    RefType = ReflectedTypeRefType.Pointer;
                    Name = $"{BaseTypeRef}*";
                    break;
                case SymTagEnum.SymTagEnum:
                    Name = symbol.name;
                    RefType = ReflectedTypeRefType.Type;
                    break;
                case SymTagEnum.SymTagBaseType:
                    Name = baseTypeMap[symbol.baseType];
                    if (Name == null) throw new NotImplementedException();
                    RefType = ReflectedTypeRefType.Primitive;
                    break;
                case SymTagEnum.SymTagFunctionType:
                    Name = "";
                    FunctionSignature = new ReflectedFunctionSignature(symbol);
                    RefType = ReflectedTypeRefType.Function;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public bool IsBasedUpon(string name)
        {
            return Name == name || (BaseTypeRef != null && BaseTypeRef.IsBasedUpon(name));
        }

        public bool IsPointerTo(string name)
        {
            return RefType == ReflectedTypeRefType.Pointer && BaseTypeRef.Name == name;
        }

        public override string ToString()
        {
            return Name;
        }

        public bool Equals(ReflectedTypeRef other)
        {
            if (RefType == ReflectedTypeRefType.Function || other.RefType == ReflectedTypeRefType.Function)
                return false; // TODO: This
            else
                return RefType == other.RefType && Name == other.Name;
        }
    }
}
