﻿using System;

using Dia2Lib;

namespace NativeReflector
{
    /// <summary>
    /// Represents a type of reflected method
    /// </summary>
    public enum ReflectedMethodType
    {
        InstanceFunction, StaticFunction,

        Constructor, Deconstructor,

        OperatorOverload
    }

    /// <summary>
    /// Represents an access level of reflected method
    /// </summary>
    public enum ReflectedMethodAccess
    {
        None, Public, Private, Protected
    }

    /// <summary>
    /// Represents a function reflected from the PDB
    /// </summary>
    public class ReflectedFunction : IEquatable<ReflectedFunction>
    {
        /// <summary>
        /// Gets the name of this function
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the qualified name of this function
        /// </summary>
        public string QualifiedName { get; private set; }

        /// <summary>
        /// Gets the type to which this function belongs
        /// </summary>
        public ReflectedType Owner { get; private set; }

        /// <summary>
        /// Gets the type of this function
        /// </summary>
        public ReflectedMethodType Type { get; private set; }

        /// <summary>
        /// Gets the access modifier of this function
        /// </summary>
        public ReflectedMethodAccess AccessModifier { get; private set; }

        /// <summary>
        /// Gets the function signature of this function
        /// </summary>
        public ReflectedFunctionSignature Signature { get; private set; }

        /// <summary>
        /// Gets if this method is virtual
        /// </summary>
        public bool IsVirtual { get; private set; }

        /// <summary>
        /// Gets if this method is pure virtual
        /// </summary>
        public bool IsPure { get; private set; }

        /// <summary>
        /// Initializes a new instance of the ReflectedType class
        /// </summary>
        /// <param name="symbol"></param>
        public ReflectedFunction(ReflectedType ownerType, IDiaSymbol symbol)
        {
            // Store name
            Name = symbol.name;
            Owner = ownerType;

            // Determine type from name
            if (ownerType != null)
            {
                const string operatorStr = "operator";
                if (Name == ownerType.Name)
                    Type = ReflectedMethodType.Constructor;
                else if (Name.Substring(1) == ownerType.Name && Name[0] == '~')
                    Type = ReflectedMethodType.Deconstructor;
                else if (Name.Length >= operatorStr.Length && Name.Substring(0, operatorStr.Length) == operatorStr)
                    Type = ReflectedMethodType.OperatorOverload;
                // TODO: Determine static function
                else
                    Type = ReflectedMethodType.InstanceFunction;
                QualifiedName = $"{ownerType.Name}::{Name}";
            }
            else
            {
                Type = ReflectedMethodType.StaticFunction;
                QualifiedName = $"{Name}";
            }

            // Load details
            LoadDetails(symbol);
        }

        #region Loading

        /// <summary>
        /// Loads all method details
        /// </summary>
        private void LoadDetails(IDiaSymbol symbol)
        {
            // Get virtual
            IsVirtual = symbol.@virtual != 0;
            IsPure = symbol.pure != 0;

            // Get access modifier
            switch (symbol.access)
            {
                case 1:
                    AccessModifier = ReflectedMethodAccess.Private;
                    break;
                case 2:
                    AccessModifier = ReflectedMethodAccess.Protected;
                    break;
                case 3:
                    AccessModifier = ReflectedMethodAccess.Public;
                    break;
            }

            // Load the signature
            IDiaSymbol sigSymbol = symbol.type;
            LoadSignature(sigSymbol);
            if (!Signature.HasThis)
                Type = ReflectedMethodType.StaticFunction;
        }

        /// <summary>
        /// Loads the function signature
        /// </summary>
        /// <param name="symbol"></param>
        private void LoadSignature(IDiaSymbol symbol)
        {
            // Create signature
            Signature = new ReflectedFunctionSignature(symbol);
        }

        #endregion

        public override string ToString()
        {
            return $"ReflectedFunction[{Signature.ReturnType} {QualifiedName}({string.Join(", ", Signature.Parameters)})]";
        }

        public bool Equals(ReflectedFunction other)
        {
            return Name == other.Name && Signature.ParametersMatch(other.Signature, true) && Signature.ReturnType.Equals(other.Signature.ReturnType);
        }
    }
}
