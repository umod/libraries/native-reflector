﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

using Dia2Lib;

namespace NativeReflector
{
    class Program
    {
        //private const string COMPILER_PATH = @"../../Oxide/Oxide.Ext.CSharp/Dependencies/CSharpCompiler.exe"
        //private const string COMPILER_PATH = @"C:\Program Files (x86)\MSBuild\14.0\Bin\csc.exe";
        private const string COMPILER_PATH = @"C:\Windows\Microsoft.NET\Framework\v3.5\csc.exe";
        private const string OUTPUT_BINARY = "../Artifacts/Oxide.ARK.dll";

        [StructLayout(LayoutKind.Sequential, Size = 8)]
        internal struct Dummy8
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] Data;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Loading PDB...");
            var dia = new DiaSourceClass();

            IDiaSession session;

            dia.loadDataFromPdb(args[0]);
            dia.openSession(out session);

            var globalScope = session.globalScope;

            var names = new HashSet<string>();
            var reflectedTypes = new List<ReflectedType>();

            Console.WriteLine("Loading types...");
            var methodCount = 0;
            var maxCount = -1;
            foreach (var typeSymbol in globalScope
                .EnumerateChildren(SymTagEnum.SymTagUDT)
                .Where(s => Filters.FilterTypeByName(s.name)))
            {
                if (!names.Contains(typeSymbol.name))
                {
                    var rType = new ReflectedType(typeSymbol);
                    reflectedTypes.Add(rType);
                    methodCount += rType.MethodCount;
                    names.Add(typeSymbol.name);
                    maxCount--;
                    if (maxCount == 0) break;
                }
            }

            const string FILE_DELEGATES = "Delegates.cs";
            const string FILE_INTERFACES = "Interfaces.cs";
            const string FILE_IMPLEMENTATIONS = "Implementations.cs";
            const string FILE_DEPENDENCIES = "NativeObject.cs Imports.cs NativePointer.cs";

            Console.WriteLine($"Loaded {reflectedTypes.Count} types with a total of {methodCount} methods.");
            Console.WriteLine("Writing bindings...");

            if (File.Exists(FILE_DELEGATES)) File.Delete(FILE_DELEGATES);
            if (File.Exists(FILE_INTERFACES)) File.Delete(FILE_INTERFACES);
            if (File.Exists(FILE_IMPLEMENTATIONS)) File.Delete(FILE_IMPLEMENTATIONS);

            FileStream delegateStream = null, interfaceStream = null, implStream = null;
            try
            {
                delegateStream = File.OpenWrite(FILE_DELEGATES);
                interfaceStream = File.OpenWrite(FILE_INTERFACES);
                implStream = File.OpenWrite(FILE_IMPLEMENTATIONS);

                using (var bindingsWriter = new BindingsWriter(delegateStream, interfaceStream, implStream))
                {
                    bindingsWriter.WriteHeaders();
                    bindingsWriter.WriteTypeBindings(reflectedTypes);
                    bindingsWriter.WriteFooters();
                }
            }
            finally
            {
                if (delegateStream != null)
                {
                    delegateStream.Close();
                    delegateStream.Dispose();
                    delegateStream = null;
                }
                if (interfaceStream != null)
                {
                    interfaceStream.Close();
                    interfaceStream.Dispose();
                    interfaceStream = null;
                }
                if (implStream != null)
                {
                    implStream.Close();
                    implStream.Dispose();
                    implStream = null;
                }
            }

            Console.WriteLine("Compiling...");
            var pStart = new ProcessStartInfo(COMPILER_PATH,
                $"/target:library /out:{OUTPUT_BINARY} {FILE_DELEGATES} {FILE_INTERFACES} {FILE_IMPLEMENTATIONS} {FILE_DEPENDENCIES}"
            );
            pStart.RedirectStandardOutput = true;
            pStart.RedirectStandardError = true;
            pStart.RedirectStandardInput = true;
            pStart.UseShellExecute = false;
            var compiler = Process.Start(pStart);
            Console.Write(compiler.StandardOutput.ReadToEnd());
            compiler.WaitForExit();
            Console.WriteLine("Done! Press Enter to continue...");
            Console.Read();
        }
    }
}
