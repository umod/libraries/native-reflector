﻿using System.Collections.Generic;

using Dia2Lib;

namespace NativeReflector
{
    /// <summary>
    /// Contains utility and extension methods for Dia classes
    /// </summary>
    public static class DiaUtils
    {
        /// <summary>
        /// Gets all children for this symbol
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="symTag"></param>
        /// <returns></returns>
        public static IEnumerable<IDiaSymbol> EnumerateChildren(this IDiaSymbol parent, SymTagEnum symTag)
        {
            IDiaEnumSymbols enumSymbols;
            parent.findChildren(symTag, null, 0, out enumSymbols);
            IDiaSymbol curSymbol;
            uint fetched;
            enumSymbols.Next(1, out curSymbol, out fetched);
            while (fetched == 1)
            {
                yield return curSymbol;
                enumSymbols.Next(1, out curSymbol, out fetched);
            }
        }
    }
}
