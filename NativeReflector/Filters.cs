﻿using System.Collections.Generic;

namespace NativeReflector
{
    /// <summary>
    /// Contains filters for reflected objects we won't deal with
    /// </summary>
    public static class Filters
    {
        private static readonly HashSet<char> acceptablePrefixes = new HashSet<char>()
        {
            'F', 'I', 'U', 'A'
        };

        public static bool FilterTypeByName(string name)
        {
            return !name.Contains("<") && !name.Contains(":") && acceptablePrefixes.Contains(name[0]);
        }
    }
}
